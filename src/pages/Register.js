import {Form, Button, Row, Col, Container} from 'react-bootstrap'


export default function Register(){
	return(
		<Container>
			<Row>
				<Col className="col-12 col-md-6">
					<Form>
						<Form.Group className="mb-3" controlId="formBasicEmail">
						  <Form.Label>First Name</Form.Label>
						  <Form.Control type="text" placeholder="Enter email" />
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicEmail">
						  <Form.Label>Last Name</Form.Label>
						  <Form.Control type="text" placeholder="Enter email" />
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicEmail">
						  <Form.Label>Email address</Form.Label>
						    <Form.Control type="email" placeholder="Enter email" />
						</Form.Group>

						  <Form.Group className="mb-3" controlId="formBasicPassword">
						    <Form.Label>Password</Form.Label>
						    <Form.Control type="password" placeholder="Password" />
						  </Form.Group>

						  <Form.Group className="mb-3" controlId="formBasicPassword">
						    <Form.Label>Confirm Password</Form.Label>
						    <Form.Control type="password" placeholder="Password" />
						  </Form.Group>

						  <Form.Group className="mb-3" controlId="formBasicCheckbox">
						    <Form.Check type="checkbox" label="Check me out" />
						  </Form.Group>
						  <Button variant="info" type="submit">
						    Submit
						  </Button>
					</Form>
				</Col>
			</Row>
		</Container>

		)
}

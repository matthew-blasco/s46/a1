import React,{Fragment} from 'react';

import AppNavbar from './../components/AppNavbar';
import Banner from './../components/Banner';
import CourseCards from './../components/CourseCards';
import Footer from './../components/Footer';





export default function Home(){
	return (
		<Fragment>
		<AppNavbar/>
		<Banner/>
		<CourseCards/>
		<Footer/> 
		</Fragment>
		)
}

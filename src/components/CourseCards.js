import{Card, Button}from 'react-bootstrap';




export default function CourseCards() {

return (
	<div className="container">
		<div className="row">
			<div className="col px-4 pb-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere</p>
				<Button variant="primary" className="mb-5">Enroll Now!</Button>
				<div className="row">
					<div className="col-12 col-md-4">
						<Card border="info" className="p-4">
						  <Card.Title>Learn From Home</Card.Title>
						  <Card.Body>
						    <Card.Text>
						      Some quick example text to build on the card title and make up the bulk of
						      the card's content.Some quick example text to build on the card title and make up the bulk of
						      the card's content.
						    </Card.Text>
						  </Card.Body>
						</Card>
					</div>
					<div className="col-12 col-md-4">
						<Card border="info" className="p-4">
						  <Card.Title>Study Now, Pay Later</Card.Title>
						  <Card.Body>
						    <Card.Text>
						      Some quick example text to build on the card title and make up the bulk of
						      the card's content.Some quick example text to build on the card title and make up the bulk of
						      the card's content.
						    </Card.Text>
						  </Card.Body>
						</Card>
					</div>
					<div className="col-12 col-md-4">
						<Card border="info" className="p-4">
						  <Card.Title>Be Part Of Our Community</Card.Title>
						  <Card.Body>
						    <Card.Text>
						      Some quick example text to build on the card title and make up the bulk of
						      the card's content.Some quick example text to build on the card title and make up the bulk of
						      the card's content.
						    </Card.Text>
						  </Card.Body>
						</Card>
					</div>
				</div>
				<div className="row mt-3">
				<div className="col-12">
					<Card border="info" className="p-2" style={{ height: '42vh' }}>
					  <Card.Title>Sample Course</Card.Title>
					  <Card.Body>
					    <Card.Text>
					    <p>Description:</p>
					    <p>This is a sample course offering.</p>
					    <p>Price:</p>
					    <p>PhP 40,000</p>
					    </Card.Text>
					    <Button variant="primary" className="mb-5">Enroll</Button>
					  </Card.Body>
					</Card>
				</div>
							
				</div>
			</div>
		</div>
	</div>
		)
}